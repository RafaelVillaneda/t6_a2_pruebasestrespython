'''
Created on 8 dic. 2020

@author: Rafael Villaneda
'''
from random import *
import time

class BinarySearch:
    def __init__(self):
        self.contador=[0,0,0]
    def mostrarContador(self):
        #1-> recorridos 2-> intercambios 3-> comparaciones
        print(f"Numeros de recorridos: {self.contador[0]}")
        print(f"Numeros de Comparaciones: {self.contador[2]}")
        self.contador[0]=0
        self.contador[2]=0
        
    def busquedaBinaria(self,array,buscado):
        inicio=0
        final=len(array)-1
        
        while inicio<= final:
            self.contador[0]=self.contador[0]+1
            puntero=(inicio+final)//2
            self.contador[2]+=1
            if buscado==array[puntero]:
                return 1
            elif buscado > array[puntero]:
                self.contador[2]+=1
                inicio=puntero+1
            else:
                final=puntero-1
        return 0
    def mezclaDirecta(self,array): 
        mitad=len(array)//2
        
        if len(array)>=2:
            arregloIz=array[mitad:]
            arregloDer=array[:mitad]

            array.clear()  
            
            self.mezclaDirecta(arregloIz)
            
            self.mezclaDirecta(arregloDer)
            
           
            while(len(arregloDer)>0 and len(arregloIz)>0):
                
                if(arregloIz[0]< arregloDer[0]):
                    array.append(arregloIz.pop(0))
                else:
                    array.append(arregloDer.pop(0))
                
            while len(arregloIz)>0:
                
                array.append(arregloIz.pop(0))
                
            
            while len(arregloDer)>0:
                array.append(arregloDer.pop(0))
        
        return array
    
            
class hash_table:
    def mostrarContador(self):
        print(f"Numeros de recorridos: {self.contador[0]}")
        print(f"Numeros de Comparaciones: {self.contador[2]}")
        self.contador[0]=0
        self.contador[2]=0
    def __init__(self):
        self.table = [None] * 127
        self.contador=[0,0,0]

    # Función hash
    def Hash_func(self, value):
        key = 0
        for i in range(0,len(value)):
            self.contador[0]=self.contador[0]+1
            key += ord(value[i])
        return key % 127

    def Insert(self, value): # Metodo para ingresar elementos
        hash = self.Hash_func(value)
        self.contador[2]+=1
        if self.table[hash] is None:
            self.table[hash] = value

    def Search(self,value): # Metodo para buscar elementos
        hash = self.Hash_func(value);
        self.contador[2]=self.contador[2]+1
        if self.table[hash] is None:
            return None
        else:
            self.mostrarContador()
            return (self.table[hash])
            

    def Remove(self,value): # Metodo para eleminar elementos
        hash = self.Hash_func(value);
        self.contador[2]+=1
        if self.table[hash] is None:
            print("No hay elementos con ese valor", value)
        else:
            print("Elemento con valor", value, "eliminado")
            self.table[hash] is None;

bandera=False
vector100E=[]
for i in range(0,100):
    vector100E.append(randint(0,100))
print(vector100E)
orden=BinarySearch()
orden2=hash_table()
for i in range(0,len(vector100E)):
    orden2.Insert(str(vector100E[i]))
orden2.mostrarContador()
print(orden2.contador)


while (bandera==False):
    tiempoIn=tFin=0
    print("Con que quieres probar?")
    print("1-> Busqueda BINARIA")
    print("2-> busqueda por hash")
    print("3-> Salir")
    op=input()
    if(op=="1"):
        num=int(input("introduce el valor que deseas buscar: "))
        vector100E=orden.mezclaDirecta(vector100E)
        tiempoIn=time.time()
        tiempoIn=time.time()
        if(orden.busquedaBinaria(vector100E, num)==1):
            tFin=time.time()
            print("Se encontro el elemento")
            print(f"Tardo: {(tFin-tiempoIn)/1000}")
            orden.mostrarContador()
        else:
            tFin=time.time()
            print("No se encontro el elemento")
            print(f"Tardo: {(tFin-tiempoIn)/1000}")
            orden.mostrarContador()
        print()
    elif(op=="2"):
        num=int(input("introduce el valor que deseas buscar: "))
        
        if(orden2.Search(str(num))!=None):
            tFin=time.time()
            print(f"Tardo: {(tFin-tiempoIn)/1000}")
            print("Se encontro el elemento")
        else:
            tFin=time.time()
            print(f"Tardo: {(tFin-tiempoIn)/1000}")
            print("No se encontro el elemento")
        print()
    elif(op=="3"):
        print("Saliendo....")
        bandera=True
    else:
        print("Elige una opcion disponible")